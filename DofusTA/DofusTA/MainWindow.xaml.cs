using IronOcr;
using Microsoft.Win32;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

using CV = OpenCvSharp;
using Point = OpenCvSharp.Point;
using Size = OpenCvSharp.Size;


namespace DofusTA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window, INotifyPropertyChanged
    {
        private const bool SHOW_INTERMEDIATE_HSV_IMAGE = false;

        private const int NB_PROCESS = 4;

        #region Fields (with DataBinding)
        private string srcPath = "";
        public string SrcPath { get => srcPath; set { srcPath = value; OnPropertyChanged(); } }
        private BitmapImage sourceImage = new BitmapImage();
        public BitmapImage SourceImage { get => sourceImage; set { sourceImage = value; OnPropertyChanged(); } }
        private int runProcessTo = 0;
        public int RunProcessTo { get => runProcessTo; set { runProcessTo = value; OnPropertyChanged(); } }
        #endregion Fields (with DataBinding)

        // Left, right
        Dictionary<LineSegmentPoint, LineSegmentPoint> lineSegmentMatchRightBest = new Dictionary<LineSegmentPoint, LineSegmentPoint>();
        // Left, top
        Dictionary<LineSegmentPoint, LineSegmentPoint?> lineSegmentMatchTopBest = new Dictionary<LineSegmentPoint, LineSegmentPoint?>();
        // Left, bottom
        Dictionary<LineSegmentPoint, LineSegmentPoint?> lineSegmentMatchBotBest = new Dictionary<LineSegmentPoint, LineSegmentPoint?>();

        public MainWindow()
        {
            InitializeComponent();

            // TODO Licence key for OCR : It's a trial licence key, available up to 11.06.2023
            // Remarks : Without license, you can use OCR in VisualStudio environnment, but not generate an exe
            IronOcr.Installation.LicenseKey = "IRONOCR.CEWAMON155.13123-40E4A68AD4-ELB2ATUYZHVB4-5KP7KMILDC5V-RD7GUCICSSNC-PBLA2GIKD55X-WSEFMAG73VKO-HVSMAY-TUBT7PH5EU2JUA-DEPLOYMENT.TRIAL-TI4DTU.TRIAL.EXPIRES.11.JUN.2023";

            Debug.WriteLine("This awesome project start here :D");

            DataContext = this;

            RunProcessTo = NB_PROCESS;
        }

        private void ButtonLoadFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFile();
            LoadFileOpened();
        }

        private void LoadFileOpened()
        {
            const string DEBUG_TAG = "[LOAD FILE] ---- ";
            bool isExist = System.IO.File.Exists(SrcPath);
            Debug.WriteLine($"{DEBUG_TAG}Is the file exists : {isExist}");
            Debug.WriteLine($"{DEBUG_TAG}\tfile path : '{SrcPath}'");

            SourceImage = new BitmapImage();
            if (!isExist)
            {
                SrcPath = "";
            }
            else
            {
                SourceImage = new BitmapImage(new Uri(SrcPath));
            }
        }

        private void OpenFile()
        {
            // Windows file dialog
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select a image";
            ofd.Filter = "PNG file (*.png)|*.png";

            bool? result = ofd.ShowDialog();
            if (result == true)
            {
                SrcPath = ofd.FileName;
            }

        }

        private void ComputeTooltipDetection(Mat src)
        {
            // Step #1 : To blackWhite
            Mat gray = ImageToGrayLevel(src);

            // Step #2 : Contours
            // https://github.com/VahidN/OpenCVSharp-Samples/blob/master/OpenCVSharpSample12/Program.cs
            Mat contoursMat = ContoursDetection(src, gray);

            // Why canny before Hough :
            // https://github.com/shimat/opencvsharp_samples/blob/master/SamplesCore/Samples/HoughLinesSample.cs
            // https://stackoverflow.com/questions/37648029/how-can-i-do-hough-transform-in-opencv-and-c

            // Step #3 : Canny
            Mat edges = CannyFilter(contoursMat);

            // Step #4 : Hough

            int minLineLength = 160;
            LineSegmentPoint[] segHough = HoughTransform(gray, edges, minLineLength);

            // Step #5 : Hough filter horitontal/verticals
            List<LineSegmentPoint> segVerticals = new List<LineSegmentPoint>();
            List<LineSegmentPoint> segHorizontals = new List<LineSegmentPoint>();

            lineSegmentMatchRightBest = new Dictionary<LineSegmentPoint, LineSegmentPoint>();
            lineSegmentMatchTopBest = new Dictionary<LineSegmentPoint, LineSegmentPoint?>();
            lineSegmentMatchBotBest = new Dictionary<LineSegmentPoint, LineSegmentPoint?>();

            HoughVerticalHorizontalFilters(gray, segHough, segVerticals, segHorizontals);

            // Step 6 : Tooltip verticals/horizontal matching
            TooltipBordersMatching(src, segVerticals, segHorizontals);
        }

        private Mat GetImageForCV(string srcPath)
        {
            const string DEBUG_TAG = "[LOAD IMAGE FOR CV] ---- ";
            if (string.IsNullOrWhiteSpace(srcPath))
            {
                Debug.WriteLine($"{DEBUG_TAG}No image selected");
                throw new Exception("No Image selected");
            }

            return Cv2.ImRead(srcPath, ImreadModes.Color);
        }

        private Mat ImageToGrayLevel(CV.Mat src)
        {
            Mat gray = new Mat();

            Cv2.CvtColor(src, gray, ColorConversionCodes.BGR2GRAY);

            return gray;
        }

        private Mat ContoursDetection(Mat src, Mat gray)
        {
            Mat grayFiltered = src.EmptyClone();
            Cv2.Threshold(gray, grayFiltered, 30, 50, ThresholdTypes.Binary);

            Mat structuringElementContours = new Mat(); // new Mat == 3x3 rect (for structuring elements)
            structuringElementContours = Mat.Ones(new Size(5, 5), MatType.CV_8U);

            Cv2.Erode(grayFiltered, grayFiltered, structuringElementContours);
            Cv2.Normalize(grayFiltered, grayFiltered, 0, 255, NormTypes.MinMax);
            if (ShowImgCvDetectionBW.IsChecked == true)
                Cv2.ImShow("black for contour", grayFiltered);

            Mat contoursMat = BuildContoursMat(grayFiltered);
            if (ShowImgCvDetectionContours.IsChecked == true)
                Cv2.ImShow("Contours", contoursMat);
            return contoursMat;
        }

        private static Mat BuildContoursMat(Mat src)
        {

            Point[][] contours; //vector<vector<Point>> contours;
            HierarchyIndex[] hierarchyIndexes; //vector<Vec4i> hierarchy;
            Cv2.FindContours(
                src,
                out contours,
                out hierarchyIndexes,
                mode: RetrievalModes.CComp,
                method: ContourApproximationModes.ApproxNone);

            var contoursMat = src.EmptyClone();

            var componentCount = 0;
            var contourIndex = 0;

            // Note : skip can improve perf, but can also decrease the ability to detect toolbox
            // But having small skips will increase the number of false detection / duplicate
            // To avoid issue, and because it's a project to "study image treatment", we will accept 0 for now
            // TODO : find a way to improve this

            //int skipNLowestContours = hierarchyIndexes.Count() / 3;
            int skipNLowestContours = 0;

            for (int skip = 0; skip < skipNLowestContours; skip++)
            {
                componentCount++;
                contourIndex = hierarchyIndexes[contourIndex].Next;
            }

            while ((contourIndex >= 0))
            {
                Cv2.DrawContours(
                    contoursMat,
                    contours,
                    contourIndex,
                    color: Scalar.All(componentCount + 1),
                    thickness: 1,
                    lineType: LineTypes.Link8,
                    hierarchy: hierarchyIndexes,
                    maxLevel: 0);

                componentCount++;
                contourIndex = hierarchyIndexes[contourIndex].Next;
            }

            return contoursMat;
        }

        private Mat CannyFilter(Mat src)
        {
            Mat edges = new Mat();

            // aperture size = Taille du noyau de convolution de sobel
            // 2+3eme param = thresholds = Seuils de détection de contours 
            Cv2.Canny(src, edges, 95, 100, apertureSize: 3);

            if (ShowImgCvCanny.IsChecked == true)
                Cv2.ImShow("Canny edges", edges);
            return edges;
        }

        private LineSegmentPoint[] HoughTransform(Mat src, Mat edges, int minLineLength)
        {
            LineSegmentPoint[] segHough = Cv2.HoughLinesP(edges, 1, Math.PI / 180, 100, minLineLength, 50);

            Mat imageHoughLines = src.EmptyClone();

            foreach (LineSegmentPoint s in segHough)
                imageHoughLines.Line(s.P1, s.P2, Scalar.White, 1, LineTypes.AntiAlias, 0);

            if (ShowImgCvHough.IsChecked == true)
                Cv2.ImShow("Hough Lines", imageHoughLines);
            return segHough;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="segments"></param>
        /// <param name="resultSegVerticals">result will be stored in it</param>
        /// <param name="resultSegHorizontals">result will be stored in it</param>
        private void HoughVerticalHorizontalFilters(Mat src, LineSegmentPoint[] segments, List<LineSegmentPoint> resultSegVerticals, List<LineSegmentPoint> resultSegHorizontals)
        {
            // H = Horizontal / V = Vertical
            Mat imageOutH = src.EmptyClone();
            Mat imageOutV = src.EmptyClone();

            foreach (LineSegmentPoint s in segments)
            {
                if (s.P1.Y == s.P2.Y) // Horizontal
                {
                    imageOutH.Line(s.P1, s.P2, Scalar.White, 1, LineTypes.AntiAlias, 0);
                    resultSegHorizontals.Add(s);
                }
                else if (s.P1.X == s.P2.X) // Vertical
                {
                    imageOutV.Line(s.P1, s.P2, Scalar.White, 1, LineTypes.AntiAlias, 0);
                    resultSegVerticals.Add(s);
                }
            }

            if (ShowImgCvHoughFilterHV.IsChecked == true)
                Cv2.ImShow("Hough Horizontal/Vertical Lines", imageOutH | imageOutV);
        }

        private void TooltipBordersMatching(Mat src, List<LineSegmentPoint> segVerticals, List<LineSegmentPoint> segHorizontals)
        {
            foreach (var seg in segVerticals)
            {
                MatchVertLineSegmentsForTooltipBest(seg, segVerticals.ToArray());
            }

            // We do a filter after the grouping for :
            // - By doing it after, we already match all possible Left/Right pairs, so we keep only one
            // - If we did it before, we can lose some possible pairs, because the filter can improve dist between left and right
            var filteredSegVerticals = GetSegmentsWithoutNearSame(
                lineSegmentMatchRightBest.Keys.ToArray(), 5, int.MaxValue); // The algo is based on the width of toolbox,
                                                                            // so we want to merge ALL near X (y maxValue means we don't care about the height))

            lineSegmentMatchRightBest.Keys.Except(filteredSegVerticals) // We keep all that pass the filter
                .ToList().ForEach(x => lineSegmentMatchRightBest.Remove(x)); // And remove all others

            var filteredSegHorizontals = GetSegmentsWithoutNearSame(segHorizontals.ToArray(), 5, 5); // We want to filter to the "same" lines => a little x y margin

            FindBestHorizontalLineForToolbox(filteredSegHorizontals.ToList(), lineSegmentMatchRightBest);

            Scalar[] colors = { Scalar.Red, Scalar.Green, Scalar.Blue, Scalar.Cyan, Scalar.Yellow, Scalar.Magenta };

            // Draw a mat with only filtered vertical lines
            Mat imgHoughFilteredVert = src.EmptyClone();
            int colorIndex = 0;
            foreach (var segDict in lineSegmentMatchRightBest)
            {
                var s1 = segDict.Key;
                var s2 = segDict.Value;
                Scalar color = colors[colorIndex];
                imgHoughFilteredVert.Line(s1.P1, s1.P2, color, 1, LineTypes.AntiAlias, 0);
                imgHoughFilteredVert.Line(s2.P1, s2.P2, color, 1, LineTypes.AntiAlias, 0);

                colorIndex++;
                colorIndex %= colors.Length;
            }

            // Draw a mat with only filtered horizontal lines
            Mat imgHoughFilteredHori = src.EmptyClone();

            colorIndex = 0;

            int nbLoop = Math.Min(lineSegmentMatchTopBest.Count, lineSegmentMatchBotBest.Count); // To avoid issue (should always be the same)
            if (lineSegmentMatchTopBest.Count != lineSegmentMatchBotBest.Count)
                Debug.WriteLine("!!!!!!!!!!!!!!WARNING !!!!!!!!!!!!!! this is not supposed to happend ==>> lineSegmentMatchTopBest.Count != lineSegmentMatchBotBest.Count");

            for (int i = 0; i < nbLoop; i++)
            {
                var s1 = lineSegmentMatchTopBest.ElementAt(i).Value ??
                    new LineSegmentPoint(new Point(0, 0), new Point(imgHoughFilteredHori.Width, 0));

                var s2 = lineSegmentMatchBotBest.ElementAt(i).Value ??
                    new LineSegmentPoint(new Point(0, imgHoughFilteredHori.Height - 1), new Point(imgHoughFilteredHori.Width, imgHoughFilteredHori.Height - 1));

                Scalar color = colors[colorIndex];
                imgHoughFilteredHori.Line(s1.P1, s1.P2, color, 1, LineTypes.AntiAlias, 0);
                imgHoughFilteredHori.Line(s2.P1, s2.P2, color, 1, LineTypes.AntiAlias, 0);
                colorIndex++;
                colorIndex %= colors.Length;
            }

            if (ShowImgCvToolboxBorder.IsChecked == true)
                Cv2.ImShow("Toolbox Horizontal/Verticals", imgHoughFilteredVert | imgHoughFilteredHori);
        }


        private string SimpleIronOcr(object input)
        { // https://ironsoftware.com/csharp/ocr/examples/csharp-configure-setup-tesseract/

            string? path = null;
            byte[]? bytes = null;

            if (input is string)
                path = (string)input;
            else if (input is byte[])
                bytes = (byte[])input;
            else
                throw new InvalidCastException("Maybe the data you pass is a valid type for IronOCR, but for this method, we only accept : string (filepath) or byte[] (data image)");

            var ocrTesseract = new IronTesseract()
            {
                Language = OcrLanguage.FrenchBest,
                Configuration = new TesseractConfiguration()
            };

            string result;

            using (var ocrInput = path != null ? // We will create a new Ocr of a specific type
                new OcrInput(path) : // path not null => using path
                new OcrInput(bytes) // else => using bytes
                )
            {
                result = ocrTesseract.Read(ocrInput).Text;
            }

            return result;
        }

        private Mat CropImg(Mat src, int startX, int startY, int endX, int endY)
        {
            // Limit to images size to avoid outofbounds
            // - First limitation on given values (to avoid negative values)
            startX = Math.Clamp(startX, 0, src.Width);
            endX = Math.Clamp(endX, 0, src.Width);
            startY = Math.Clamp(startY, 0, src.Height);
            endY = Math.Clamp(endY, 0, src.Height);

            // - Second limitation (to avoid image outbounds)
            endX = Math.Clamp(endX - startX, 0, src.Width - startX); // end-start because Rect use length instead of end points
            endY = Math.Clamp(endY - startY, 0, src.Height - startY);

            OpenCvSharp.Rect cropped = new OpenCvSharp.Rect(startX, startY, endX, endY);
            Mat resizedSrc = new Mat(src, cropped);
            return resizedSrc;
        }

        private ItemData ExtractItemDataFromImage(Mat src, string imageName)
        {
            Mat hsvSrc = src.EmptyClone();
            Cv2.CvtColor(src, hsvSrc, ColorConversionCodes.BGR2HSV);

            // Hue Angle value https://www.tydac.ch/color/
            // Hue is in 0-180 !! => /2 (cf : https://redrainkim.github.io/opencvsharp/opencvsharp-study-13/)

            // Note we start with "Green" to be consistant with the priority of the wanted stats of item toolbox
            Scalar hsvGreenMin = new Scalar(100 / 2, 0.4 * 255, 0.2 * 255);
            Scalar hsvGreenMax = new Scalar(160 / 2, 1 * 255, 1 * 255);
            Mat g_readable = ExtractWithHSV(hsvGreenMin, hsvGreenMax, src,
                "green", imageName, ShowImgCvToolboxForHsvNormal.IsChecked == true);

            Scalar hsvRedMin = new Scalar(0 / 2, 0.4 * 255, 0.2 * 255);
            Scalar hsvRedMax = new Scalar(40 / 2, 1 * 255, 1 * 255);
            Mat r_readable = ExtractWithHSV(hsvRedMin, hsvRedMax, src,
                "red", imageName, ShowImgCvToolboxForHsvNegative.IsChecked == true);

            Scalar hsvBlueMin = new Scalar(200 / 2, 0.4 * 255, 0.2 * 255);
            Scalar hsvBlueMax = new Scalar(260 / 2, 1 * 255, 1 * 255);
            Mat b_readable = ExtractWithHSV(hsvBlueMin, hsvBlueMax, src,
                "blue", imageName, ShowImgCvToolboxForHsvExo.IsChecked == true);

            Scalar hsvGrayMin = new Scalar(0 / 2, 0 * 255, 0.4 * 255);
            Scalar hsvGrayMax = new Scalar(360 / 2, 0.1 * 255, 1 * 255);
            Mat gray_readable = ExtractWithHSV(hsvGrayMin, hsvGrayMax, src,
                "gray", imageName, ShowImgCvToolboxForHsvGray.IsChecked == true);

            string grayData = SimpleIronOcr(gray_readable.ToBytes());

            return new ItemData()
            {
                ItemName = ExtractItemName(grayData),
                NormalStats = SimpleIronOcr(g_readable.ToBytes()),
                ExoStats = SimpleIronOcr(b_readable.ToBytes()),
                NegativeStats = SimpleIronOcr(r_readable.ToBytes()),
                GrayData = grayData,
            };
        }

        private string ExtractItemName(string fullData)
        {
            // TODO, for now we asume it's always one line (for some items, it could be splitted on 2 lines, but it is realy rare

            // Sample of bad extracted :
            // - "...\r\n[Title]" => we got some "... noise" (replace ... by anything)
            // - "\r\n[Title]" => No noise got, but it's not on the first line

            // We know the item name is always on the line before the level !

            // Regex based on sample : https://stackoverflow.com/questions/9436381/c-sharp-regex-string-extraction
            string pattern = @"^(?<title>[^\r\n]*)\r\nNiveau";
            Match match = Regex.Match(fullData, pattern, RegexOptions.Multiline);

            if (match.Success)
            {
                return match.Groups["title"].Value;
            }

            // We have add the regex check before to avoid if name is bad extracted (see comments above)
            return fullData.Substring(0, fullData.IndexOf("\r\n"));
        }

        private Mat ExtractWithHSV(Scalar hsvMin, Scalar hsvMax, Mat src, string colorName, string imageName, bool allowShow)
        {
            // HSV Inspiration source : https://medium.com/featurepreneur/colour-filtering-and-colour-pop-effects-using-opencv-python-3ce7d4576140

            Mat hsvSrc = src.EmptyClone();
            Cv2.CvtColor(src, hsvSrc, ColorConversionCodes.BGR2HSV);


            Mat mask = hsvSrc.EmptyClone();
            Cv2.InRange(hsvSrc, hsvMin, hsvMax, mask);

            if (ShowImgCvToolboxToHsv.IsChecked == true && allowShow)
                Cv2.ImShow($"src in HSV for {imageName}", hsvSrc);

            if (ShowImgCvToolboxToHsvMask.IsChecked == true && allowShow)
                Cv2.ImShow($"{colorName} : HSV mask produced in for {imageName}", mask);

            if (ShowImgCvToolboxToHsvResult.IsChecked == true && allowShow)
            {
                // No really need to get the image in color, mask is self usable for OCR
                // It's just to see the plain result :D
                Mat hsvGreenMat = src.EmptyClone();
                Cv2.BitwiseAnd(src, src, hsvGreenMat, mask);
                Cv2.ImShow($"{colorName} : result after applyed HSV mask for {imageName}", hsvGreenMat);
            }


            return mask;
        }

        private void ChangeBestRightMatchingLines(LineSegmentPoint line, LineSegmentPoint other)
        {
            const string DEBUG_TAG = "[BEST VERT] ---- ";

            int dist = Math.Abs(line.P1.X - other.P1.X);

            if (!lineSegmentMatchRightBest.ContainsKey(line))
            {
                lineSegmentMatchRightBest.Add(line, other);
                Debug.WriteLine($"{DEBUG_TAG}Found a match of lines for [" + line.P1.X + "] at [" + other.P1.X + "] with [" + dist + "] distance");
            }
            else
            {
                int currentDist = Math.Abs(line.P1.X - lineSegmentMatchRightBest[line].P1.X);
                if (dist < currentDist)
                {
                    lineSegmentMatchRightBest[line] = other;
                    Debug.WriteLine($"{DEBUG_TAG}Found best a match of lines for [" + line.P1.X + "] at [" + other.P1.X + "] with [" + dist + "] distance.");
                    Debug.WriteLine($"{DEBUG_TAG}\tPrevious dist was = {currentDist}");
                }
            }
        }

        private void MatchVertLineSegmentsForTooltipBest(LineSegmentPoint toCheck, LineSegmentPoint[] lines)
        {
            int wantedDist = 455;
            int tolerence = 5; // Note : 15 is good too, but for "img1" we got a false positive.
                               // Based on the full hough verticals, the image 3 will always give 2/3 items, we can't do better.

            // Note : first condition removed to avoid duplicate due to negative (we already had scanned them in positive)
            var linesToCompareV2 = lines.OrderBy(s => s.P1.X)
                .Where(s =>
                        //(s.P1.X >= toCheck.P1.X - wantedDist - tolerence
                        // && s.P1.X <= toCheck.P1.X - wantedDist + tolerence)
                        //||
                        (s.P1.X >= toCheck.P1.X + wantedDist - tolerence
                         && s.P1.X <= toCheck.P1.X + wantedDist + tolerence)
                    );

            foreach (var line in linesToCompareV2)
            {
                int dist = Math.Abs(line.P1.X - toCheck.P1.X);

                ChangeBestRightMatchingLines(toCheck, line);
            }
        }

        private LineSegmentPoint[] GetSegmentsWithoutNearSame(LineSegmentPoint[] segments, int sameTolerenceX, int sameTolerenceY)
        {
            Func<LineSegmentPoint, LineSegmentPoint, bool> whereLineWidth = (current, next) =>
                Math.Abs(next.P1.X - current.P1.X) < sameTolerenceX
                || Math.Abs(next.P2.X - current.P2.X) < sameTolerenceX
                || Math.Abs(next.P1.X - current.P2.X) < sameTolerenceX // p1 p2 because we can have the x inverted between 2 lines
                || Math.Abs(next.P2.X - current.P1.X) < sameTolerenceX;

            Func<LineSegmentPoint, LineSegmentPoint, bool> whereLineHeight = (current, next) =>
                Math.Abs(next.P1.Y - current.P1.Y) < sameTolerenceY
                || Math.Abs(next.P2.Y - current.P2.Y) < sameTolerenceY
                || Math.Abs(next.P1.Y - current.P2.Y) < sameTolerenceY // p1 p2 because we can have the y inverted between 2 lines
                || Math.Abs(next.P2.Y - current.P1.Y) < sameTolerenceY;
            // See capture "Filtre ligneVert (avec filtre Y)" for without this condition
            // And "Filtre ligneVert (sans filtre Y)" for with this condition

            var linesToCheck = segments.ToList().OrderBy(s => s.P1.X);
            var linesToRemove = linesToCheck
                .Zip(linesToCheck.Skip(1), (current, next) => new { current, next }) // Group current and next
                .Where(pair => whereLineWidth(pair.current, pair.next)
                    && whereLineHeight(pair.current, pair.next)) // Check if "near" same value
                .Select(pair => pair.current.P1.X < pair.next.P1.X ? pair.current : pair.next); // keep the smallest of nearest

            List<LineSegmentPoint> result = new List<LineSegmentPoint>();
            foreach (var line in segments)
            {
                if (!linesToRemove.Contains(line))
                {
                    result.Add(line);
                }
            }

            return result.ToArray();
        }

        private Mat[] CropDetectedTooltips(Mat src)
        {
            const string DEBUG_TAG = "[CROP] ---- ";
            int nbImages = lineSegmentMatchRightBest.Count;

            if (nbImages == 0)
            {
                Cv2.ImShow($"{DEBUG_TAG} Image item not cropped (no tooltip detected)", src);

                return new Mat[] { src };
            }

            Mat[] images = new Mat[nbImages];

            int imgIndex = 0;
            foreach (var segGroup in lineSegmentMatchRightBest)
            {
                int margin = 10;

                LineSegmentPoint leftSeg = segGroup.Key;
                LineSegmentPoint rightSeg = segGroup.Value;

                LineSegmentPoint? topSeg = lineSegmentMatchTopBest.TryGetValue(leftSeg, out var tmpTryResult) ? tmpTryResult : null;
                LineSegmentPoint? botSeg = lineSegmentMatchBotBest.TryGetValue(leftSeg, out tmpTryResult) ? tmpTryResult : null;

                int left = leftSeg.P1.X - margin;
                int right = rightSeg.P1.X + margin;
                int top = topSeg?.P1.Y - margin ?? 0;
                int bot = botSeg?.P2.Y + margin ?? src.Height;

                Debug.WriteLine($"{DEBUG_TAG}l: {leftSeg} // r: {rightSeg} // t: {topSeg} // b: {botSeg}");
                Debug.WriteLine($"{DEBUG_TAG}\t => l={left} // r={right} // t={top} // b={bot}");

                images[imgIndex] = CropImg(src, left, top, right, bot);
                imgIndex++;
            }

            // Show images
            if (ShowImgCvCroppedToolbox.IsChecked == true)
            {
                for (int i = 0; i < images.Length; i++)
                {
                    Cv2.ImShow($"Image item {i + 1}", images[i]);
                }
            }

            return images;
        }

        private void FindBestHorizontalLineForToolbox(List<LineSegmentPoint> possibleHorizontal, Dictionary<LineSegmentPoint, LineSegmentPoint> verticalsGroup)
        {
            const string DEBUG_TAG = "[FIND HORI] ---- ";

            // First line of the vertical group, lists of possible horizontal
            Dictionary<LineSegmentPoint, List<LineSegmentPoint>> availableTopHorizontals = new Dictionary<LineSegmentPoint, List<LineSegmentPoint>>();
            Dictionary<LineSegmentPoint, List<LineSegmentPoint>> availableBotHorizontals = new Dictionary<LineSegmentPoint, List<LineSegmentPoint>>();

            foreach (var segGroup in verticalsGroup)
            {
                int margin = 0;

                LineSegmentPoint startVert = segGroup.Key;
                LineSegmentPoint endVert = segGroup.Value;

                int xStart = startVert.P1.X - margin;
                int xEnd = endVert.P1.X + margin;

                // Note : P1 and P2 y can be inversed, like this debug output
                // Seg 1 (P1:(x:861 y:398) P2:(x:861 y:7)) // Seg 2 (P1:(x:1318 y:390) P2:(x:1318 y:8))
                int[] possibleY = new[] { startVert.P1.Y, startVert.P2.Y, endVert.P1.Y, endVert.P2.Y };

                int yStart = possibleY.Min() - margin;
                int yEnd = possibleY.Max() + margin;

                // Filter lines to exclude the one outside the vertical group
                var filteredHLines = possibleHorizontal
                    .Where(line => (line.P1.X >= xStart && line.P1.X <= xEnd) // p1 is in the range
                        || (line.P2.X >= xStart && line.P2.X <= xEnd) // p2 is in the range
                        || (line.P1.X < xStart && line.P2.X > xEnd) // p1 and p2 are outside the range (traversent line)
                    ).ToList();

                Debug.WriteLine($"{DEBUG_TAG}//////////////////////////////////////////////////////////////////////");
                Debug.WriteLine($"{DEBUG_TAG}grp " + startVert + " // " + endVert);
                Debug.WriteLine($"{DEBUG_TAG}\t" + $" (x,y) start ({xStart}, {yStart}) >> end ({xEnd}, {yEnd})");

                availableTopHorizontals.Add(startVert, new List<LineSegmentPoint>());
                availableBotHorizontals.Add(startVert, new List<LineSegmentPoint>());

                foreach (var hSeg in filteredHLines)
                {
                    if (IsHLineTop(yStart, yEnd, hSeg))
                        availableTopHorizontals[startVert].Add(hSeg);
                    else
                        availableBotHorizontals[startVert].Add(hSeg);
                }

                LineSegmentPoint? bestHorizontal;
                // Top process
                bestHorizontal = GetBestHorizontalLineForGroup(startVert, endVert, availableTopHorizontals[startVert], true);
                lineSegmentMatchTopBest.Add(startVert, bestHorizontal);

                // Bottom process
                bestHorizontal = GetBestHorizontalLineForGroup(startVert, endVert, availableBotHorizontals[startVert], false);
                lineSegmentMatchBotBest.Add(startVert, bestHorizontal);
            }
        }

        private bool IsHLineTop(int yStart, int yEnd, LineSegmentPoint hLine)
        {
            var yPos = hLine.P1.Y;

            bool isTop;
            if (yEnd > yStart)
                isTop = yPos <= ((yEnd - yStart) / 2) + yStart; // +yStart to adjust origin
            else
                isTop = yPos <= ((yStart - yEnd) / 2) + yStart;

            return isTop;
        }

        private LineSegmentPoint? GetBestHorizontalLineForGroup(LineSegmentPoint vertical1, LineSegmentPoint vertical2, List<LineSegmentPoint> horizontalCandidats, bool isTop)
        {
            const string DEBUG_TAG = "[BEST HORI] ---- ";
            string topBot = isTop ? "TOP" : "BOT";
            Debug.WriteLine($"{DEBUG_TAG}Process to find best {topBot} candidat");

            if (horizontalCandidats.Count == 0)
            {
                Debug.WriteLine($"{DEBUG_TAG}\tNot able to find a best candidate, there is no one");
                return null;
            }

            if (horizontalCandidats.Count == 1)
            {
                Debug.WriteLine($"{DEBUG_TAG}\t1 candidat => no #2 filter");
                return horizontalCandidats[0];
            }

            // Step 2 >1 candidat ? => keep the nearest of bottom/top of verticals
            // Note, this is not necessarly the best choice, see image "Best Horizontal Candidat Step2" (we take the green)
            Debug.WriteLine($"{DEBUG_TAG}\tThere is always candidats, filter #2");

            int[] verticalPoints = new[] { vertical1.P1.Y, vertical1.P2.Y, vertical2.P1.Y, vertical2.P2.Y };

            int wantedVerticalPos = isTop ? verticalPoints.Min() : verticalPoints.Max();

            // Get the nearest (best candidat)
            var bestHorizontal = horizontalCandidats.OrderBy(s => Math.Abs(s.P1.Y - wantedVerticalPos)).First();
            Debug.WriteLine($"{DEBUG_TAG}\t===>>> Best candidate is {bestHorizontal}");

            return bestHorizontal;
        }

        private void BuildTabResult(ItemData[] datas)
        {
            TabItemsResult.Items.Clear();
            int itemIndex = 0;
            foreach (ItemData data in datas)
            {
                itemIndex++;

                TabItem tabItem = new TabItem();

                ItemDataTemplate itemUI = new ItemDataTemplate();

                itemUI.SetData(data);

                tabItem.Header = $"Item {itemIndex}";
                tabItem.Content = itemUI;

                TabItemsResult.Items.Add(tabItem);
            }

            if (itemIndex > 0)
                TabItemsResult.SelectedIndex = 0;


        }

        private void ButtonFullProcess_Click(object sender, RoutedEventArgs e)
        {
            RunProcess(NB_PROCESS);
        }

        private void RunProcess(int processTo)
        {
            if (processTo < 1)
            {
                MessageBox.Show("No process to run. Select higher value (ie : 2 means it will run process 1 & 2)",
                    "Cannot start process",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrEmpty(SrcPath))
            {
                MessageBox.Show("Please load a valid image first",
                    "Cannot start process",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Mat src = GetImageForCV(SrcPath);

            // Step 1 : Groups (canny, hough, grouping match, etc)
            ComputeTooltipDetection(src);

            if (processTo == 1)
                return;

            int nbTooltips = lineSegmentMatchRightBest.Count;

            // Step 2 : Crop tooltips
            Mat[] images = CropDetectedTooltips(src);

            if (processTo == 2)
                return;

            // Step 3 : OCR
            ItemData[] itemDatas = new ItemData[nbTooltips];
            for (int i = 0; i < nbTooltips; i++)
            {
                itemDatas[i] = ExtractItemDataFromImage(images[i], $"image {i + 1}");
            }

            if (processTo == 3)
                return;

            // Step 4 : Clear and build item tabs to the result tab
            BuildTabResult(itemDatas);
        }

        #region Interface implementation
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(
               [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion Interface implementation

        #region UseFull methods but not used anymore
        private Mat HistogramImage(CV.Mat src)
        {
            const string DEBUG_TAG = "[HISTOGRAMME] ---- ";

            Mat histResult;

            Mat histo = new Mat();
            int[] histSize = { 256 };
            Rangef[] ranges = { new Rangef(0, 256) };
            int[] channels = { 0 };
            var dimension = 1; // 1D

            Cv2.CalcHist(new[] { src }, channels, null, histo, dimension, histSize, ranges);

            Debug.WriteLine($"{DEBUG_TAG}Hist calculated");

            // Get histogram image source : https://github.com/VahidN/OpenCVSharp-Samples/blob/master/OpenCVSharpSample10/Program.cs
            using (var histogramImage = (CV.Mat)(CV.Mat.Ones(rows: src.Rows, cols: src.Cols, type: MatType.CV_8U) * 255))
            {
                // Scales and draws histogram

                Cv2.Normalize(histo, histo, 0, histogramImage.Rows, NormTypes.MinMax);

                var color = Scalar.All(100);

                for (var i = 0; i < histSize[0]; i++)
                {
                    var binW = Math.Round((double)histogramImage.Cols / histSize[0], MidpointRounding.ToEven);

                    Cv2.Rectangle(histogramImage,
                    new Point(i * binW, histogramImage.Rows),
                        new Point((i + 1) * binW, histogramImage.Rows - Math.Round(histo.Get<float>(i), MidpointRounding.ToEven)),
                        color,
                        -1);
                }

                histResult = histogramImage.Clone();
                Debug.WriteLine($"{DEBUG_TAG}Hist image ready to print");
            }

            return histResult;
        }

        private string ComputeTesseract(Mat img)
        { // sample usage from https://www.tech-quantum.com/ocr-text-using-opencv-and-tesseract/
            string tessDataPath = DownloadAndExtractLanguagePack();
            string result = "";
            OpenCvSharp.Rect[] textLocations = null;
            string[] componentTexts = null;
            float[] confidences = null;
            using (var engine = OpenCvSharp.Text.OCRTesseract.Create(tessDataPath, "fra"))
            {
                engine.Run(img, out result, out textLocations, out componentTexts, out confidences, OpenCvSharp.Text.ComponentLevels.TextLine);
            }

            return result;
        }

        private static string DownloadAndExtractLanguagePack()
        { // code of this method fully copied from https://www.tech-quantum.com/ocr-text-using-opencv-and-tesseract/
          // Except zip file name updated to the last version
          // TODO : Maybe do not DL the zip and only include french in app 

            const string DEBUG_TAG = "[DL Tesseract Data] - ";

            int versionToUse = 4; // cf switch order

            string tessVer = "";
            string langPackPath = "";

            switch (versionToUse)
            {
                case 0:
                    // Tesseract 3.4
                    tessVer = "tess3_4";
                    langPackPath = "https://github.com/tesseract-ocr/tessdata/archive/refs/tags/3.04.00.zip";
                    break;
                case 1:
                    // Tesseract 4.0
                    tessVer = "tess4_0";
                    langPackPath = "https://github.com/tesseract-ocr/tessdata/archive/refs/tags/4.0.0.zip";
                    break;
                case 2:
                    // Tesseract 4.1
                    tessVer = "tess4_1";
                    langPackPath = "https://github.com/tesseract-ocr/tessdata/archive/refs/tags/4.1.0.zip";
                    break;
                case 3:
                    // Tesseract fast 4.1
                    tessVer = "tessFast4_1";
                    langPackPath = "https://github.com/tesseract-ocr/tessdata_fast/archive/refs/tags/4.1.0.zip";
                    break;
                default:
                    // Tesseract best 4.1
                    tessVer = "tessBest4_1";
                    langPackPath = "https://github.com/tesseract-ocr/tessdata_best/archive/refs/tags/4.1.0.zip";
                    break;
            }


            //Source path to the zip file
            string tessFolder = AppDomain.CurrentDomain.BaseDirectory + "\\" + tessVer;
            string zipFileName = tessFolder + "\\tessdata.zip";
            Debug.WriteLine(DEBUG_TAG + zipFileName);
            string tessDataFolder = tessFolder + "\\tessdata"; ;
            //Check and download the source file
            if (!Directory.Exists(tessFolder))
            {
                Directory.CreateDirectory(tessFolder);
            }

            if (!File.Exists(zipFileName))
            {
                WebClient client = new WebClient();
                client.DownloadFile(langPackPath, zipFileName);
            }
            //Extract the zip to tessdata folder
            if (!string.IsNullOrWhiteSpace(tessDataFolder) && !Directory.Exists(tessDataFolder))
            {
                ZipFile.ExtractToDirectory(zipFileName, AppDomain.CurrentDomain.BaseDirectory);
                var extractedDir = Directory.EnumerateDirectories(AppDomain.CurrentDomain.BaseDirectory).FirstOrDefault(x => (x.Contains("tessdata")));
                Directory.Move(extractedDir, tessDataFolder);
            }
            return tessDataFolder;
        }

        #endregion UseFull methods but not used anymore

        private void LoadedImageSource_MouseDown(object sender, System.Windows.Input.MouseEventArgs e)
        {
            OpenFolder(Path.GetDirectoryName(SrcPath));
        }

        private void OpenFolder(string folderPath)
        { // Source (100%) https://www.codeproject.com/Questions/852563/How-to-open-file-explorer-at-given-location-in-csh
            if (Directory.Exists(folderPath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    Arguments = folderPath,
                    FileName = "explorer.exe"
                };

                Process.Start(startInfo);
            }
            else
            {
                MessageBox.Show(string.Format("{0} Directory does not exist!", folderPath));
            }
        }

        private void ButtonPartialProcess_Click(object sender, RoutedEventArgs e)
        {
            RunProcess(RunProcessTo);
        }
    }
}
