﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DofusTA
{
    public class ItemData
    {
        public string ItemName { get; set; } = "";
        public string NormalStats { get; set; } = "";
        public string ExoStats { get; set; } = "";
        public string NegativeStats { get; set; } = "";
        public string GrayData { get; set; } = "";
    }
}
