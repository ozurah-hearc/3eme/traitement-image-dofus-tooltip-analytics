﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DofusTA
{
    // TODO NOT FINISHED
    // Missing some resisrance types (like % on spells)
    // And all weapon damages types

    public enum StatType
    {
        Unknown,

        // Primary
        ActionPoint,
        MovementPoint,
        Range,
        Vitality,
        Agility,
        Chance,
        Strength,
        Intel,
        Power,
        Crit,
        Wisdom,

        // Secondary
        ReductionActionPoint, // retrait PA
        ParryActionPoint, // Esquive PA
        ReductionMovementPoint,
        parryMovementPoint,
        Heal,
        Lock, // Tacle
        Dodge,  // Fuite
        Initiative,
        Summons,
        Prospecting,

        // Damage
        Damage,
        DamageCritical,
        DamageFixedNeutral,
        DamageFixedEarth,
        DamageFixedFire,
        DamageFixedWater,
        DamageFixedAir,
        DamageReflect,
        DamageFixedTraps,
        DamagePowerTraps,
        DamagePushback, // poussée
        DamagesOnSpell,
        DamagesOnWeapon,
        DamageRanged,
        DamageMelee,


        // Resistance

        ResistanceFixedNeutral,
        ResistancePercentNeutral,
        ResistanceFixedEarth,
        ResistancePercentEarth,
        ResistanceFixedFire,
        ResistancePercentFire,
        ResistanceFixedWater,
        ResistancePercentWater,
        ResistanceFixedAir,
        ResistancePercentAir,
        ResistanceFixedCritical,
        ResistanceFixedPushback,
    }

    public static class StatTypeExtension
    {
        public static string TextValue(this StatType type)
        {
            // All text value are in french cause the text we want to analyse from image is in french
            return type switch
            {
                StatType.Unknown => "Unknown", // Default value

                StatType.ActionPoint => "PA",
                StatType.MovementPoint => "PM",
                StatType.Range => "Portée",
                StatType.Vitality => "Vitalité",
                StatType.Agility => "Agilité",
                StatType.Chance => "Chance",
                StatType.Strength => "Force",
                StatType.Intel => "Intelligence",
                StatType.Power => "Puissance",
                StatType.Crit => "Critique",
                StatType.Wisdom => "Sagesse",

                StatType.ReductionActionPoint => "Retrait PA",
                StatType.ParryActionPoint => "Esquive PA",
                StatType.ReductionMovementPoint => "Retrait PM",
                StatType.parryMovementPoint => "Esquive PM",
                StatType.Heal => "Soins",
                StatType.Lock => "Tacle",
                StatType.Dodge => "Fuite",
                StatType.Initiative => "Initiative",
                StatType.Summons => "Invocation",
                StatType.Prospecting => "Prospection",

                StatType.Damage => "Dommages",
                StatType.DamageCritical => "Dommages Critiques",
                StatType.DamageFixedNeutral => "Dommages Neutre",
                StatType.DamageFixedEarth => "Dommages Terre",
                StatType.DamageFixedFire => "Dommages Feu",
                StatType.DamageFixedWater => "Dommages Eau",
                StatType.DamageFixedAir => "Dommages Air",
                StatType.DamageReflect => StatType.Unknown.TextValue(), // The text is special, and there is only 1 item => skipped
                StatType.DamageFixedTraps => "Dommages Pièges",
                StatType.DamagePowerTraps => "Puissance (pièges)",
                StatType.DamagePushback => "Dommages Poussée",
                StatType.DamagesOnSpell => "% Dommages aux sorts",
                StatType.DamagesOnWeapon => "% Dommages d'armes",
                StatType.DamageRanged => "% Dommages distance",
                StatType.DamageMelee => "% Dommages mêlée",

                StatType.ResistanceFixedNeutral => "Résistance Neutre",
                StatType.ResistancePercentNeutral => "% Résistance neutre",
                StatType.ResistanceFixedEarth => "Résistance Terre",
                StatType.ResistancePercentEarth => "% Résistance Terre",
                StatType.ResistanceFixedFire => "Résistance Feu",
                StatType.ResistancePercentFire => "% Résistance Feu",
                StatType.ResistanceFixedWater => "Résistance Eau",
                StatType.ResistancePercentWater => "% Résistance Eau",
                StatType.ResistanceFixedAir => "Résistance Air",
                StatType.ResistancePercentAir => "% Résistance Air",
                
                StatType.ResistanceFixedCritical => "Résistances Critiques",
                StatType.ResistanceFixedPushback => "Résistances poussée",

                _ => StatType.Unknown.TextValue(),
            };
        }
    }
}
