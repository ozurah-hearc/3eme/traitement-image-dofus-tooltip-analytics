﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DofusTA
{
    /// <summary>
    /// Logique d'interaction pour ItemData.xaml
    /// </summary>
    public partial class ItemDataTemplate : UserControl
    {
        public ItemDataTemplate()
        {
            InitializeComponent();
        }

        public void SetData(ItemData item)
        {
            SetData(item.ItemName, item.GrayData, item.NormalStats, item.ExoStats, item.NegativeStats);
        }

        public void SetData(string name, string grayData, string normalStats, string exoStats, string negativeStats)
        {
            ItemName.Text = name;
            DataGray.Text = grayData;
            StatsNormal.Text = normalStats;
            StatsExo.Text = exoStats;
            StatsNeg.Text = negativeStats;
        }
    }
}
