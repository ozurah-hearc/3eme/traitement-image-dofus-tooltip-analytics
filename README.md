# grp05_DofusTooltipAnalytics_Allemann-Chappuis

Projet réalisé par Allemann Jonas et Chappuis Sébastien dans le cadre du cours de traitement d'images II à la HE-Arc Ingénierie.

**Dofus Tooltip Analytics**

Comparateur de statistiques d'items du jeu Dofus.

[Lien vers le wiki][wiki]

Le guide d'utilisation du projet se trouve dans le [rapport][rapport] (chapitre 4) (disponible sur le [wiki][wiki])

Le projet se trouve dans le dossier "[DofusTA][projet]", il s'agit d'une solution Visual Studio 2022.

Les images de tests à utiliser avec l'application se trouvent dans le dossier "[Images][testImg]".

[projet]: https://gitlab-etu.ing.he-arc.ch/isc/2022-23/niveau-3/3292.1-traitement-images-ii/isc3il-a/grp05_dofustooltipanalytics_allemann-chappuis/-/tree/main/DofusTA

[testImg]: https://gitlab-etu.ing.he-arc.ch/isc/2022-23/niveau-3/3292.1-traitement-images-ii/isc3il-a/grp05_dofustooltipanalytics_allemann-chappuis/-/tree/main/Images

[rapport]: https://gitlab-etu.ing.he-arc.ch/isc/2022-23/niveau-3/3292.1-traitement-images-ii/isc3il-a/grp05_dofustooltipanalytics_allemann-chappuis/-/wikis/Export/Rapport.pdf

[wiki]: https://gitlab-etu.ing.he-arc.ch/isc/2022-23/niveau-3/3292.1-traitement-images-ii/isc3il-a/grp05_dofustooltipanalytics_allemann-chappuis/-/wikis/home